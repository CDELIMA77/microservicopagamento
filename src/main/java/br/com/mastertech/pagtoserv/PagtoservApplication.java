package br.com.mastertech.pagtoserv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PagtoservApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagtoservApplication.class, args);
	}

}
