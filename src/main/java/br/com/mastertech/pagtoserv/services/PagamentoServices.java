package br.com.mastertech.pagtoserv.services;

import br.com.mastertech.pagtoserv.clients.CartaoClient;
import br.com.mastertech.pagtoserv.exceptions.ManipuladorDeExcecoes;
import br.com.mastertech.pagtoserv.models.Dtos.CartaoDTO;
import br.com.mastertech.pagtoserv.models.Dtos.PagamentoDTO;
import br.com.mastertech.pagtoserv.models.Pagamento;
import br.com.mastertech.pagtoserv.repositories.PagamentoRepository;
import feign.FeignException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoServices {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoclient;

    public List<Pagamento> consultarPagamentoPorCartao(Long id) throws ObjectNotFoundException {
        List<Pagamento> listaPagamentos = pagamentoRepository.findByCartaoid(id);
        if (listaPagamentos.size() > 0) {
            return listaPagamentos;
        } else {
            throw new ObjectNotFoundException(CartaoDTO.class, "Pagamentos para este cartão não encontrados");
        }
    }

    public Pagamento salvarPagamento(PagamentoDTO pagamentoDTO) throws ObjectNotFoundException {
        try {/* Criou nova rota para conseguir buscar pelo número e saber se esta ativo, o q nao existia
        no contrato atual*/
            CartaoDTO cartaoDTO = cartaoclient.getCartaoById(pagamentoDTO.getCartao_id());
            if (cartaoDTO.isAtivo()){
            Pagamento pagamento = new Pagamento();
            pagamento.setCartaoid(cartaoDTO.getId());
            pagamento.setDescricao(pagamentoDTO.getDescricao());
            pagamento.setValor(pagamentoDTO.getValor());
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;}
            else {
                throw new ObjectNotFoundException(Pagamento.class, "Cartão não está ativo");
            }
        } catch (FeignException.BadRequest e) {
            throw new ManipuladorDeExcecoes.CartaoNotFoundException();
        }
    }
}