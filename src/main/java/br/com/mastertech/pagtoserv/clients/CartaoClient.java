package br.com.mastertech.pagtoserv.clients;

import br.com.mastertech.pagtoserv.models.Dtos.CartaoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {
    @GetMapping("/cartao/id/{id}")
    CartaoDTO getCartaoById(@PathVariable Long id);
}
