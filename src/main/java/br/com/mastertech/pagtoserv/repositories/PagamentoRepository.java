package br.com.mastertech.pagtoserv.repositories;


import br.com.mastertech.pagtoserv.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findByCartaoid(Long cartaoid);
}