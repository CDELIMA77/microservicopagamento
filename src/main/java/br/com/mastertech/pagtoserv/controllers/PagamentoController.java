package br.com.mastertech.pagtoserv.controllers;

import br.com.mastertech.pagtoserv.clients.CartaoClient;
import br.com.mastertech.pagtoserv.models.Dtos.PagamentoDTO;
import br.com.mastertech.pagtoserv.models.Pagamento;
import br.com.mastertech.pagtoserv.services.PagamentoServices;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoServices pagamentoServices;

    @Autowired
    private CartaoClient cartaoClient;

    @GetMapping("pagamentos/{id_cartao}")
    public ResponseEntity<List<Pagamento>> buscarPagamento(@PathVariable Long id_cartao){
            List<Pagamento> pagamentoLista;
            try{pagamentoLista = pagamentoServices.consultarPagamentoPorCartao(id_cartao);}
            catch (ObjectNotFoundException e){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            }
            return ResponseEntity.status(200).body(pagamentoLista);
        }

    @PostMapping("pagamento")
    public ResponseEntity<Pagamento> incluirPagamento(@RequestBody PagamentoDTO pagamentoDTO) {
        Pagamento pagamentoObjeto = pagamentoServices.salvarPagamento(pagamentoDTO);
        return ResponseEntity.status(201).body(pagamentoObjeto);
    }
}
