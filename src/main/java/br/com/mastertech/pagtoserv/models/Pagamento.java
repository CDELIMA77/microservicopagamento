package br.com.mastertech.pagtoserv.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "pagamentoms")
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @JsonProperty("cartao_id")
    private Long cartaoid;

    @Column
    private String descricao;

    @Column
    private double valor;

    public Pagamento() {
    }

    public Pagamento(Long id, Long cartaoid, String descricao, double valor) {
        this.id = id;
        this.cartaoid = cartaoid;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCartaoid() {
        return cartaoid;
    }

    public void setCartaoid(Long cartaoid) {
        this.cartaoid = cartaoid;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}


