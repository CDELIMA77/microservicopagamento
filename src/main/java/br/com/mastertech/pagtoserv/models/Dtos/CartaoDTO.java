package br.com.mastertech.pagtoserv.models.Dtos;

public class CartaoDTO {

    private Long id;
    private String numero;
    private Long cliente_id;
    private boolean ativo;

    public CartaoDTO() {
    }

    public CartaoDTO(Long id, String numero, Long cliente_id, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.cliente_id = cliente_id;
        this.ativo = ativo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
